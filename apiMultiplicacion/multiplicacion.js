const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let respuesta = {
    error: false,
    codigo: 200,
    mensaje: ''
   };

app.get('/multiplicacion', function (req, res) {
  res.send('[GET]Operacion multiplicacion');
});

app.post('/multiplicacion', function (req, res) {
    if(!req.body.valor1 || !req.body.valor2) {
      respuesta = {
      error: true,
      codigo: 502,
      mensaje: 'El campo valor1 y valor2 son requeridos'
     };
    }else{
            if( !isNaN(parseInt(req.body.valor1 )) && !isNaN(parseInt(req.body.valor2))) {
                respuesta = {
                error: false,
                codigo: 200,
                mensaje: 'Resultado operacion multiplicacion:' + (parseInt(req.body.valor1 )*parseInt(req.body.valor2))
                };
            } else {
                respuesta = {
                error: true,
                codigo: 503,
                mensaje: 'Formato no valido para operacion',
               
                };
            }
    }
    res.send(respuesta);
   });