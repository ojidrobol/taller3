import React, { Component } from 'react';
import { render } from 'react-dom'


class App extends Component{
  constructor(args) {
    super(args)
    this.state = {
      valor1: '1',
      valor2: '2',
      resultado: []
    }

  }

  enviar(e){
    e.preventDefault()
   
  
    var urls = {mult: 'http://ec2-18-228-12-180.sa-east-1.compute.amazonaws.com/multiplicacion',
                divi: 'http://ec2-18-222-106-127.us-east-2.compute.amazonaws.com/division',
                suma: 'http://localhost:3001/suma',
                resta: 'http://ec2-54-232-138-221.sa-east-1.compute.amazonaws.com/multiplicacion'};
    console.log(this.state)
    var data = {valor1: this.state.valor1, valor2: this.state.valor2}
    console.log(data)
    fetch(urls[this.state.oper], {
      method: 'POST',
      headers:{ 'Content-Type': "application/json" },
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then( (resultado) => {
        this.setState({resultado: resultado})
      })      
      .then(response => console.log('Success:', response))
      .catch(error => console.error('Error:', error));
     
  }


  onChange(e){
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  render() {
    return (      
      <form onSubmit={ this.enviar.bind(this) } >
       <input 
          value={this.state.dato1} 
          onChange={ this.onChange.bind(this) }
          type="text" name="valor1" id="valor1" />
        <input 
          value={this.state.dato2} 
          onChange={ this.onChange.bind(this) }
          type="text" name="valor2" id="valor2" />
        <div></div>
        <input           
          type="radio" name="oper" value="suma" 
          onChange={ this.onChange.bind(this) }/> Suma
        <div></div>
        <input type="radio" name="oper" value="resta" 
          onChange={ this.onChange.bind(this) }/> Resta
        <div/>
        <input type="radio" name="oper" value="mult"
          onChange={ this.onChange.bind(this) }/> Multiplicación
        <div></div>
        <input type="radio" name="oper" value="divi"
          onChange={ this.onChange.bind(this) }/> División
        <div>
          <input            
            type="submit" 
            value="calcular" />
        </div>

        <span>{JSON.stringify(this.state)}</span>
    
     </form>
      
    )
  }
}

render(<App />, document.getElementById('root'))