const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let respuesta = {
    error: false,
    codigo: 200,
    mensaje: ''
   };

app.get('/suma', function (req, res) {
  res.send('[GET]Operacion suma');
});

app.post('/suma', function (req, res) {
    if(!req.body.valor1 || !req.body.valor2) {
     respuesta = {
      error: true,
      codigo: 502,
      mensaje: 'El campo valor1 y valor2 son requeridos'
     };
    } else {
            if( !isNaN(parseInt(req.body.valor1 )) && !isNaN(parseInt(req.body.valor2))) {
                respuesta = {
                error: false,
                codigo: 200,
                mensaje: 'Resultado operacion suma:' + (parseInt(req.body.valor1 )+parseInt(req.body.valor2))
                };
            } else {
                respuesta = {
                error: true,
                codigo: 503,
                mensaje: 'Formato no valido para operacion',
               
                };
            }
    }
    
    res.send(respuesta);
   });

app.use(function(req, res, next) {
    respuesta = {
     error: true, 
     codigo: 404, 
     mensaje: 'URL no encontrada'
    };
    res.status(404).send(respuesta);
   });

app.listen(3001, () => {
 console.log("El servidor está inicializado en el puerto 3001");
});